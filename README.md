# gogs-docker

Repo to host your Gogs infrastructure using docker-compose on your machine.

## Getting Started

After cloning the repo you can simply run this project using the command:

DRONE_HOST=http://gogs-web docker-compose up

the variable DRON_HOST must be configured as a dns entry or a a /etc/hosts entry.

Remember that when installing Gogs, at the beginning, the application url must be the DRONE_HOST:3000 or a different port. You do not have to repeat this process everytime you start the server (docker-compose up).

Regarding dokuwiki the port that is listening to is 8080. Remember to set the chmod of the dokuwiki directory to rwxrwxrwx and delete the .gitkeep file otherwise it will crash.
