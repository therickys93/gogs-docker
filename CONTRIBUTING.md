# Contributing

If you want to contribute to this project please fork the repo and create a merge request with the new feature, bug fixes and so on. Contribution are really welcomed!
